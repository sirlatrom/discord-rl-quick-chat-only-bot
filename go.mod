module gitlab.com/sirlatrom/discord-rl-quick-chat-only-bot

// +heroku goVersion 1.23
go 1.23

require (
	github.com/bwmarrin/discordgo v0.28.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/handlers v1.5.2
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/securecookie v1.1.2
	github.com/gorilla/sessions v1.4.0
	github.com/sirupsen/logrus v1.9.3
	github.com/urfave/cli/v2 v2.27.5
	gopkg.in/boj/redistore.v1 v1.0.0-20160128113310-fc113767cd6b
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/garyburd/redigo v1.6.4 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.31.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/crypto v0.28.0 // indirect
	golang.org/x/net v0.29.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
