FROM golang:1.23.4-alpine AS build
WORKDIR /src
COPY go.* /src/
RUN go mod download
COPY *.go /src/
RUN CGO_ENABLED=0 go build -o /usr/bin/discord-rl-quick-chat-only-bot

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /usr/bin/discord-rl-quick-chat-only-bot /usr/bin/discord-rl-quick-chat-only-bot
ENTRYPOINT ["/usr/bin/discord-rl-quick-chat-only-bot"]