package main

import (
	"context"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis"
	"github.com/gorilla/csrf"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gopkg.in/boj/redistore.v1"
)

// Variables used for command line parameters
var (
	version         string
	allowedMessages = []string{
		"$#@%!",
		"All yors.",
		"Bumping!",
		"Calclated.",
		"Centering!",
		"Close one!",
		"Defending...",
		"Great pass!",
		"I got it!",
		"My bad...",
		"No problem.",
		"Noooo!",
		"OMG!",
		"On your left.",
		"On your right.",
		"Passing!",
		"Sorry!",
		"Take the shot!",
		"Thanks!",
		"Whew.",
		"Whoops...",
		"Wow!",

		"What a save!",
		"Nice shot!",

		"All yours.",
		"Calculated.",
		"Go for it!",
		"Great Clear!",
		"Holy cow!",
		"In position.",
		"Incoming!",
		"My bad…",
		"My fault.",
		"Need Boost!",
		"Nice Block!",
		"Nice one!",
		"No Way!",
		"Okay.",
		"Oops!",
		"Savage!",
		"Siiiick!",
		"What a play!",
		"Whew.",

		"Everybody dance!",
		"gg",
		"Nice moves.",
		"One. More. Game.",
		"Rematch!",
		"That was fun!",
		"Well played.",
		"What a Game!",

		"Bumping!",
		"Faking.",
		"On your left.",
		"On your right.",
		"Passing!",
		"Nice Bump!",
		"Nice Demo!",

		"Good luck!",
		"Have fun!",
		"Get ready.",
		"This is Rocket League!",
		"Let's do this!",
		"Here. We. Go.",
		"Nice cars!",
		"I'll do my best.",
	}
	allowedMessageMap = map[string]struct{}{}
)

type cooldown time.Duration

func (c *cooldown) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%q", time.Duration(*c).String())), nil
}

func (c *cooldown) UnmarshalJSON(data []byte) error {
	if data[0] != '"' || data[len(data)-1] != '"' {
		nanos, err := strconv.Atoi(string(data))
		if err != nil {
			return fmt.Errorf("not a string and not a number: %q: %v", string(data), err)
		}
		*c = cooldown(time.Duration(nanos))
		return nil
	}
	dataString := strings.TrimSuffix(strings.TrimPrefix(string(data), `"`), `"`)
	d, err := time.ParseDuration(dataString)
	if err != nil {
		return err
	}
	*c = cooldown(d)
	return nil
}

const (
	frontpagePath     = "/"
	authPath          = "/auth"
	apiPrefix         = "/api"
	endpointsPath     = apiPrefix + "/endpoints"
	mePath            = apiPrefix + "/me"
	settingsPath      = apiPrefix + "/settings"
	userGuildsPath    = apiPrefix + "/user-guilds"
	guildChannelsPath = apiPrefix + "/guild-channels"
	guildEmojisPath   = apiPrefix + "/guild-emojis"
	logoutPath        = "/logout"
)

type endpoints struct {
	ClientID      string `json:"client_id,omitempty"`
	Scheme        string `json:"scheme,omitempty"`
	Auth          string `json:"auth,omitempty"`
	Me            string `json:"me,omitempty"`
	Settings      string `json:"settings,omitempty"`
	UserGuilds    string `json:"user_guilds,omitempty"`
	GuildChannels string `json:"guild_channels,omitempty"`
	GuildEmojiS   string `json:"guild_emojis,omitempty"`
	Frontpage     string `json:"frontpage,omitempty"`
	Logout        string `json:"logout,omitempty"`
}

type appConfig struct {
	Token          string `json:"token,omitempty"`
	ClientID       string `json:"client_id,omitempty"`
	ClientSecret   string `json:"client_secret,omitempty"`
	ChannelID      string `json:"channel_id,omitempty"`
	RedisAddr      string `json:"redis_addr,omitempty"`
	SelfDomain     string `json:"self_domain,omitempty"`
	ExternalScheme string `json:"external_scheme,omitempty"`

	BotSettings        map[string]*botSettings `json:"bot_settings,omitempty"`
	DefaultBotSettings botSettings             `json:"default_bot_settings,omitempty"`
	Endpoints          endpoints               `json:"endpoints,omitempty"`
}

type botSettings struct {
	ChannelID           string   `json:"channel_id,omitempty"`
	VotenoEmojiID       string   `json:"voteno_emoji_id,omitempty"`
	SpamCooldownInitial cooldown `json:"spam_cooldown_initial,omitempty"`
	SpamCooldownMax     cooldown `json:"spam_cooldown_max,omitempty"`
	DeleteDelay         cooldown `json:"delete_delay,omitempty"`
}

type userWithAvatarURLType struct {
	*discordgo.User
	AvatarURL string `json:"avatar_url,omitempty"`
}

type userGuildWithIconURL struct {
	*discordgo.UserGuild
	IconURL string `json:"icon_url,omitempty"`
}

type emojiResolvedType struct {
	*discordgo.Emoji
	MessageFormat string `json:"message_format,omitempty"`
	APIName       string `json:"api_name,omitempty"`
	URL           string `json:"url,omitempty"`
}

// spaHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type spaHandler struct {
	staticPath string
	indexPath  string
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	appConfig := appConfig{
		BotSettings: map[string]*botSettings{},
	}

	logger := logrus.New()
	logger.Level = logrus.DebugLevel
	log := logger.WithFields(logrus.Fields{})

	app := cli.NewApp()
	app.Version = version
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "token",
			EnvVars:     []string{"DISCORD_TOKEN"},
			Destination: &appConfig.Token,
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "client-id",
			EnvVars:     []string{"CLIENT_ID"},
			Destination: &appConfig.ClientID,
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "client-secret",
			EnvVars:     []string{"CLIENT_SECRET"},
			Destination: &appConfig.ClientSecret,
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "self-domain",
			EnvVars:     []string{"SELF_DOMAIN"},
			Destination: &appConfig.SelfDomain,
			Required:    true,
		},
		&cli.StringFlag{
			Name:        "external-scheme",
			EnvVars:     []string{"EXTERNAL_SCHEME"},
			Destination: &appConfig.ExternalScheme,
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "channel",
			EnvVars:     []string{"CHANNEL_ID"},
			Destination: &appConfig.ChannelID,
			Required:    false,
		},
		&cli.StringFlag{
			Name:        "redis-addr",
			EnvVars:     []string{"REDIS_ADDR"},
			Destination: &appConfig.RedisAddr,
			Value:       "redis:6379",
		},
		&cli.StringFlag{
			Name:        "voteno-emoji-id",
			EnvVars:     []string{"VOTENO_EMOJI_ID"},
			Destination: &appConfig.DefaultBotSettings.VotenoEmojiID,
			Value:       ":voteno:758088444244262922",
		},
		&cli.DurationFlag{
			Name:    "spam-cooldown-initial",
			EnvVars: []string{"SPAM_COOLDOWN_INITIAL"},
			Value:   2 * time.Second,
		},
		&cli.DurationFlag{
			Name:    "spam-cooldown-max",
			EnvVars: []string{"SPAM_COOLDOWN_MAX"},
			Value:   5 * time.Second,
		},
		&cli.DurationFlag{
			Name:    "delete-delay",
			EnvVars: []string{"DELETE_DELAY"},
			Value:   2 * time.Second,
		},
	}
	app.Before = func(c *cli.Context) error {
		appConfig.DefaultBotSettings.SpamCooldownInitial = cooldown(c.Duration("spam-cooldown-initial"))
		appConfig.DefaultBotSettings.SpamCooldownMax = cooldown(c.Duration("spam-cooldown-max"))
		appConfig.DefaultBotSettings.DeleteDelay = cooldown(c.Duration("delete-delay"))

		selfDomain := appConfig.SelfDomain
		if appConfig.ExternalScheme != "" {
			selfDomain = fmt.Sprintf("%s://%s", appConfig.ExternalScheme, selfDomain)
		}
		selfDomainURL, err := url.Parse(selfDomain)
		if err != nil {
			return fmt.Errorf("parsing self-domain: %v", err)
		}
		appConfig.Endpoints = endpoints{
			Scheme:        selfDomainURL.Scheme,
			ClientID:      appConfig.ClientID,
			Frontpage:     (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: frontpagePath}).String(),
			Auth:          (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: authPath}).String(),
			Me:            (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: mePath}).String(),
			Settings:      (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: settingsPath}).String(),
			UserGuilds:    (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: userGuildsPath}).String(),
			GuildChannels: (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: guildChannelsPath}).String(),
			GuildEmojiS:   (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: guildEmojisPath}).String(),
			Logout:        (&url.URL{Scheme: selfDomainURL.Scheme, Host: selfDomainURL.Host, Path: logoutPath}).String(),
		}

		for _, m := range allowedMessages {
			allowedMessageMap[m] = struct{}{}
		}
		return nil
	}
	app.Action = func(c *cli.Context) error {
		// Create a new Discord session using the provided bot token.
		dg, err := discordgo.New("Bot " + appConfig.Token)
		if err != nil {
			return fmt.Errorf("creating Discord session: %v", err)
		}
		dg.StateEnabled = true

		redisClient := redis.NewClient(&redis.Options{
			Addr: appConfig.RedisAddr,
		})

		guildSettingsKeys, err := redisClient.Keys("guild_settings:*").Result()
		if err != nil {
			log.Errorf("error getting guild settings keys from Redis: %v", err)
		}
		log.Infof("Loaded %v guild settings from Redis", len(guildSettingsKeys))
		for _, key := range guildSettingsKeys {
			parts := strings.SplitN(key, ":", 2)
			if len(parts) != 2 {
				continue
			}
			guildID := parts[1]
			settingsJSON, err := redisClient.Get(key).Result()
			if err != nil {
				return fmt.Errorf("error getting guild settings from Redis: %v", err)
			}
			log.Infof("Settings string: %v", settingsJSON)
			unmarshaled := &botSettings{}
			if err := json.Unmarshal([]byte(settingsJSON), unmarshaled); err != nil {
				log.Errorf("error unmarshaling guild settings from Redis: %v", err)
				continue
			}
			appConfig.BotSettings[guildID] = unmarshaled
			guild, err := dg.Guild(guildID)
			if err != nil {
				return fmt.Errorf("error looking up guild by id %v: %v", guildID, err)
			}
			log.Infof("Loaded guild settings for guild %v", guild.ID)
		}
		if appConfig.ChannelID != "" {
			channel, err := dg.Channel(appConfig.ChannelID)
			if err != nil {
				return fmt.Errorf("getting channel by ID %v: %v", appConfig.ChannelID, err)
			}
			settings := appConfig.DefaultBotSettings
			appConfig.BotSettings[channel.GuildID] = &settings
		}

		sessionKey, err := redisClient.Get("SessionKey").Result()
		if err != nil {
			log.Warnf("No existing session key, generating new")
			sessionKey = string(securecookie.GenerateRandomKey(32))
			if err != redis.Nil {
				log.Warnf("error getting session key from Redis: %v", err)
			}
			if _, err := redisClient.Set("SessionKey", sessionKey, 0).Result(); err != nil {
				log.Warnf("error setting session key in Redis: %v", err)
			}
		}

		var store sessions.Store
		store, err = redistore.NewRediStore(10, "tcp", appConfig.RedisAddr, "", []byte(sessionKey))
		if err != nil {
			store = sessions.NewCookieStore([]byte(sessionKey))
			log.Warnf("creating Redis session store: %v", err)
		}
		if _, ok := store.(io.Closer); ok {
			defer store.(io.Closer).Close()
		}
		httpHandler := httpHandlerType{
			ctx:            ctx,
			l:              log,
			cancel:         cancel,
			s:              dg,
			config:         &appConfig,
			c:              http.DefaultClient,
			store:          store,
			redis:          redisClient,
			botAdminGuilds: map[string][]userGuildWithIconURL{},
			guildChannels:  map[string]map[string][]*discordgo.Channel{},
		}
		gob.Register(&discordgo.User{})
		gob.Register(map[string]userGuildWithIconURL{})
		gob.Register(map[string][]*discordgo.Channel{})

		r := mux.NewRouter()

		r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
			json.NewEncoder(w).Encode(map[string]bool{"ok": true})
		})

		r.Path(endpointsPath).HandlerFunc(httpHandler.endpointsHandler)
		r.Path(authPath).HandlerFunc(httpHandler.authHandler)
		r.Path(mePath).HandlerFunc(httpHandler.meHandler)
		r.Path(settingsPath).HandlerFunc(httpHandler.settingsHandler)
		r.Path(userGuildsPath).HandlerFunc(httpHandler.userGuildsHandler)
		r.Path(guildChannelsPath).HandlerFunc(httpHandler.guildChannelsHandler)
		r.Path(guildEmojisPath).HandlerFunc(httpHandler.guildEmojisHandler)
		r.Path(logoutPath).HandlerFunc(httpHandler.logoutHandler)
		spa := spaHandler{staticPath: "build", indexPath: "index.html"}
		r.PathPrefix(frontpagePath).Handler(spa)

		// csrfKey := securecookie.GenerateRandomKey(32)
		// CSRF := csrf.Protect([]byte(csrfKey))
		headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
		originsOk := handlers.AllowedOrigins([]string{appConfig.SelfDomain, "discord.com"})
		methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
		srv := &http.Server{
			// Handler: CSRF(handlers.CORS(originsOk, headersOk, methodsOk)(r)),
			Handler: handlers.CORS(originsOk, headersOk, methodsOk)(r),
			Addr:    ":8080",
			// Good practice: enforce timeouts for servers you create!
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		}

		go srv.ListenAndServe()

		censor := rlCensor{
			ctx:    ctx,
			l:      log,
			cancel: cancel,

			settings: appConfig.BotSettings,

			cooldowns: map[string]time.Time{},
			handlerC:  make(chan messageInSession, 50),
			redis:     redisClient,
		}

		// Register the messageCreate func as a callback for MessageCreate events.
		dg.AddHandler(censor.messageCreate)

		// In this bot, we only care about receiving message events.
		dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages | discordgo.IntentsAllWithoutPrivileged | discordgo.IntentMessageContent)

		// Open a websocket connection to Discord and begin listening.
		err = dg.Open()
		if err != nil {
			return fmt.Errorf("opening connection: %v", err)
		}

		// Wait here until CTRL-C or other term signal is received.
		log.Infof("Bot is now running. Press CTRL-C to exit.")
		sc := make(chan os.Signal, 1)
		signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

		go censor.process(ctx)
	outer:
		for {
			select {
			case <-sc:
				fmt.Println()
				break outer
			case <-ctx.Done():
				break outer
			}
		}
		cancel()
		log.Infof("Exiting")

		// Cleanly close down the Discord session.
		if err := dg.Close(); err != nil {
			return fmt.Errorf("closing: %v", err)
		}
		log.Infof("Closed Discord connection")
		return nil
	}
	if err := app.RunContext(ctx, os.Args); err != nil {
		log.Fatalf("error: %v", err)
	}
}

type httpHandlerType struct {
	ctx    context.Context
	l      *logrus.Entry
	cancel context.CancelFunc
	s      *discordgo.Session
	config *appConfig
	store  sessions.Store
	redis  *redis.Client

	botAdminGuilds map[string][]userGuildWithIconURL
	guildChannels  map[string]map[string][]*discordgo.Channel

	c *http.Client
}

type tokenResponseType struct {
	AccessToken  string `json:"access_token,omitempty"`
	TokenType    string `json:"token_type,omitempty"`
	ExpiresIn    int64  `json:"expires_in,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
	Scope        string `json:"scope,omitempty"`
}

func (h httpHandlerType) endpointsHandler(wr http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		http.Error(wr, "Only GET allowed", http.StatusMethodNotAllowed)
		return
	}
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	if err := json.NewEncoder(wr).Encode(h.config.Endpoints); err != nil {
		http.Error(wr, "error writing response", http.StatusInternalServerError)
		return
	}
}

func (h httpHandlerType) authHandler(wr http.ResponseWriter, r *http.Request) {
	accessCode := r.URL.Query().Get("code")

	tokenURL := `https://discordapp.com/api/oauth2/token`
	values := url.Values{}
	values.Set("client_id", h.config.ClientID)
	values.Set("client_secret", h.config.ClientSecret)
	values.Set("grant_type", "authorization_code")
	values.Set("redirect_uri", h.config.Endpoints.Auth)
	values.Set("code", accessCode)
	values.Set("scope", "identify")

	resp, err := http.PostForm(tokenURL, values)
	if err != nil {
		http.Error(wr, fmt.Sprintf("error during token request: %v", err), http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != 200 {
		errorResponse, _ := io.ReadAll(resp.Body)
		http.Error(wr, fmt.Sprintf("unexpected status code %v during token request: %v", resp.StatusCode, string(errorResponse)), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	var tokenResponse tokenResponseType
	if err := decoder.Decode(&tokenResponse); err != nil {
		http.Error(wr, fmt.Sprintf("error decoding token response: %v", err), http.StatusInternalServerError)
		return
	}

	meURL := url.URL{
		Scheme: "https",
		Host:   "discordapp.com",
		Path:   "/api/users/@me",
	}
	req, err := http.NewRequest(http.MethodGet, meURL.String(), nil)
	if err != nil {
		http.Error(wr, fmt.Sprintf("error building @me request: %v", err), http.StatusInternalServerError)
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("%s %s", tokenResponse.TokenType, tokenResponse.AccessToken))
	resp, err = h.c.Do(req)
	if err != nil {
		http.Error(wr, fmt.Sprintf("error during @me request: %v", err), http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != 200 {
		errResponse, err := io.ReadAll(resp.Body)
		if err != nil {
			http.Error(wr, fmt.Sprintf("error reading @me body: %v", err), http.StatusInternalServerError)
			return
		}
		http.Error(wr, fmt.Sprintf("@me response: %v", string(errResponse)), http.StatusInternalServerError)
		return
	}
	me := &discordgo.User{}
	decoder = json.NewDecoder(resp.Body)
	if err := decoder.Decode(me); err != nil {
		http.Error(wr, fmt.Sprintf("error decoding @me response: %v", err), http.StatusInternalServerError)
		return
	}

	session, _ := h.store.Get(req, "session-id")
	session.Values["user"] = me

	if err := session.Save(r, wr); err != nil {
		http.Error(wr, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := h.refreshGuildsInfo(me); err != nil {
		http.Error(wr, fmt.Sprintf("error getting user guilds: %v", err), http.StatusInternalServerError)
		return
	}

	wr.Header().Set("Location", frontpagePath)
	wr.WriteHeader(302)
}

func (h httpHandlerType) refreshGuildsInfo(me *discordgo.User) error {
	botAdminGuilds := []userGuildWithIconURL{}
	guildChannels := map[string][]*discordgo.Channel{}
	guilds, err := h.s.UserGuilds(100, "", "", false)
	if err != nil {
		return fmt.Errorf("error getting user guilds: %v", err)
	}
	for _, userGuild := range guilds {
		guild, err := h.s.Guild(userGuild.ID)
		if err != nil {
			return err
		}
		channels, err := h.s.GuildChannels(userGuild.ID)
		if err != nil {
			return err
		}
		for _, channel := range channels {
			h.l.Infof("me: %#v, channel: %#v", me, channel)
			permissions, err := h.s.UserChannelPermissions(me.ID, channel.ID)
			if err != nil {
				return err
			}
			h.l.Infof("Permissions for %v on %v: %v", me.Username, channel.Name, permissions)
			guildChannels[userGuild.ID] = append(guildChannels[userGuild.ID], channel)
		}
		botAdminGuilds = append(botAdminGuilds, userGuildWithIconURL{UserGuild: userGuild, IconURL: guild.IconURL("16")})
	}
	h.botAdminGuilds[me.ID] = botAdminGuilds
	h.guildChannels[me.ID] = guildChannels
	return nil
}

func (h httpHandlerType) meHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if session.IsNew {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	userRaw, exists := session.Values["user"]
	if !exists {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	user, ok := userRaw.(*discordgo.User)
	if !ok {
		http.Error(wr, "error converting user from session object", http.StatusInternalServerError)
		return
	}
	userWithAvatarURL := userWithAvatarURLType{
		user,
		user.AvatarURL("32"),
	}
	if err := json.NewEncoder(wr).Encode(userWithAvatarURL); err != nil {
		http.Error(wr, "error writing response", http.StatusInternalServerError)
		return
	}
}

func (h httpHandlerType) logoutHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	switch req.Method {
	case http.MethodPost:
		session.Options.MaxAge = -1
		if err := session.Save(req, wr); err != nil {
			http.Error(wr, fmt.Sprintf("error saving session object: %v", err), http.StatusInternalServerError)
			return
		}
	default:
		http.Error(wr, "Only POST allowed", http.StatusMethodNotAllowed)
		return
	}
}

func (h httpHandlerType) settingsHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if session.IsNew {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	userRaw, exists := session.Values["user"]
	if !exists {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	user, ok := userRaw.(*discordgo.User)
	if !ok {
		http.Error(wr, "error converting user from session object", http.StatusInternalServerError)
		return
	}
	botAdminGuilds, exists := h.botAdminGuilds[user.ID]
	if !exists {
		if err := h.refreshGuildsInfo(user); err != nil {
			http.Error(wr, fmt.Sprintf("unable to refresh guilds info for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
		botAdminGuilds, exists = h.botAdminGuilds[user.ID]
		if !exists {
			http.Error(wr, fmt.Sprintf("unable to find guilds for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
	}

	query := req.URL.Query()
	guildID := query.Get("guild_id")
	var guildForSettings *discordgo.UserGuild
	for _, userGuild := range botAdminGuilds {
		if userGuild.ID == guildID {
			guildForSettings = userGuild.UserGuild
			break
		}
	}
	if guildForSettings == nil {
		http.Error(wr, fmt.Sprintf("unable to find guild %v in which user %v#%v has admin permissions", guildID, user.Username, user.Discriminator), http.StatusInternalServerError)
		return
	}

	switch req.Method {
	case http.MethodGet:
		wr.Header().Set("Content-Type", "application/json")
		settings, exists := h.config.BotSettings[guildForSettings.ID]
		if !exists {
			// Copy default settings
			defaultSettings := h.config.DefaultBotSettings
			settings = &defaultSettings
		}
		if err := json.NewEncoder(wr).Encode(settings); err != nil {
			http.Error(wr, err.Error(), http.StatusInternalServerError)
		}
		return
	case http.MethodPost:
		var guildSettings botSettings = h.config.DefaultBotSettings
		decoder := json.NewDecoder(req.Body)
		if err := decoder.Decode(&guildSettings); err != nil {
			http.Error(wr, fmt.Sprintf("error parsing settings: %v", err), 400)
			return
		}
		if err := h.config.saveBotSettings(guildID, guildSettings); err != nil {
			http.Error(wr, fmt.Sprintf("error saving settings: %v", err), 400)
			return
		}
		marshaled, err := json.Marshal(guildSettings)
		if err != nil {
			http.Error(wr, fmt.Sprintf("error marshaling settings: %v", err), http.StatusInternalServerError)
			return
		}
		if _, err := h.redis.Set(fmt.Sprintf("guild_settings:%s", guildID), string(marshaled), 0).Result(); err != nil {
			http.Error(wr, fmt.Sprintf("error saving settings in Redis: %v", err), http.StatusInternalServerError)
			return
		}
		h.l.Infof("Saved guild settings for guild %v", guildID)
		wr.WriteHeader(202)
		return
	default:
		http.Error(wr, "Only GET, POST allowed", http.StatusMethodNotAllowed)
		return
	}
}

func (h httpHandlerType) userGuildsHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if session.IsNew {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}

	userRaw, exists := session.Values["user"]
	if !exists {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	user, ok := userRaw.(*discordgo.User)
	if !ok {
		http.Error(wr, "error converting user from session object", http.StatusInternalServerError)
		return
	}

	botAdminGuilds, exists := h.botAdminGuilds[user.ID]
	if !exists {
		if err := h.refreshGuildsInfo(user); err != nil {
			http.Error(wr, fmt.Sprintf("unable to refresh guilds info for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
		botAdminGuilds, exists = h.botAdminGuilds[user.ID]
		if !exists {
			http.Error(wr, fmt.Sprintf("unable to find guilds for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
	}

	switch req.Method {
	case http.MethodGet:
		wr.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(wr).Encode(botAdminGuilds); err != nil {
			http.Error(wr, err.Error(), http.StatusInternalServerError)
			return
		}
	default:
		http.Error(wr, "Only GET allowed", http.StatusMethodNotAllowed)
		return
	}
}

func (h httpHandlerType) guildChannelsHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if session.IsNew {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}

	userRaw, exists := session.Values["user"]
	if !exists {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	user, ok := userRaw.(*discordgo.User)
	if !ok {
		http.Error(wr, "error converting user from session object", http.StatusInternalServerError)
		return
	}

	guildChannels, exists := h.guildChannels[user.ID]
	if !exists {
		h.l.Errorf("unable to find guilds channel for user %v#%v among %v", user.Username, user.Discriminator, h.guildChannels)
		http.Error(wr, fmt.Sprintf("unable to find guilds channels for user %v#%v", user.Username, user.Discriminator), http.StatusInternalServerError)
		return
	}

	switch req.Method {
	case http.MethodGet:
		wr.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(wr).Encode(guildChannels); err != nil {
			http.Error(wr, err.Error(), http.StatusInternalServerError)
			return
		}
	default:
		http.Error(wr, "Only GET allowed", http.StatusMethodNotAllowed)
		return
	}
}

func (h httpHandlerType) guildEmojisHandler(wr http.ResponseWriter, req *http.Request) {
	wr.Header().Set("X-CSRF-Token", csrf.Token(req))
	session, err := h.store.Get(req, "session-id")
	if err != nil {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if session.IsNew {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}

	userRaw, exists := session.Values["user"]
	if !exists {
		http.Error(wr, "Unauthorized", http.StatusUnauthorized)
		return
	}
	user, ok := userRaw.(*discordgo.User)
	if !ok {
		http.Error(wr, "error converting user from session object", http.StatusInternalServerError)
		return
	}

	botAdminGuilds, exists := h.botAdminGuilds[user.ID]
	if !exists {
		if err := h.refreshGuildsInfo(user); err != nil {
			http.Error(wr, fmt.Sprintf("unable to refresh guilds info for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
		botAdminGuilds, exists = h.botAdminGuilds[user.ID]
		if !exists {
			http.Error(wr, fmt.Sprintf("unable to find guilds for user %v#%v: %v", user.Username, user.Discriminator, err), http.StatusInternalServerError)
			return
		}
	}

	query := req.URL.Query()
	guildID := query.Get("guild_id")
	var guildForEmoji *discordgo.UserGuild
	for _, userGuild := range botAdminGuilds {
		if userGuild.ID == guildID {
			guildForEmoji = userGuild.UserGuild
			break
		}
	}
	if guildForEmoji == nil {
		http.Error(wr, fmt.Sprintf("unable to find guild %v in which user %v#%v has admin permissions", guildID, user.Username, user.Discriminator), http.StatusInternalServerError)
		return
	}

	switch req.Method {
	case http.MethodGet:
		wr.Header().Set("Content-Type", "application/json")
		emojis, err := h.s.GuildEmojis(guildID)
		if err != nil {
			http.Error(wr, fmt.Sprintf("error getting guild emojis: %v", err), http.StatusInternalServerError)
			return
		}
		emojisResolved := []emojiResolvedType{}
		for _, emoji := range emojis {
			emojisResolved = append(emojisResolved, emojiResolvedType{
				Emoji:         emoji,
				APIName:       emoji.APIName(),
				MessageFormat: emoji.MessageFormat(),
				URL:           fmt.Sprintf("%semojis/%s.png?v=1", discordgo.EndpointCDN, emoji.ID),
			})
		}

		if err := json.NewEncoder(wr).Encode(emojisResolved); err != nil {
			http.Error(wr, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (a appConfig) saveBotSettings(guildID string, settings botSettings) error {
	if settings.SpamCooldownMax < settings.SpamCooldownInitial {
		return fmt.Errorf("max spam cooldown must be greater than or equal to initial spam cooldown")
	}
	a.BotSettings[guildID] = &settings
	return nil
}

type messageInSession struct {
	s *discordgo.Session
	m *discordgo.Message
}

type rlCensor struct {
	ctx    context.Context
	l      *logrus.Entry
	cancel context.CancelFunc

	settings map[string]*botSettings

	handlerC  chan messageInSession
	cooldowns map[string]time.Time
	redis     *redis.Client
}

func (r rlCensor) process(ctx context.Context) error {
	for {
		select {
		case mis := <-r.handlerC:
			if cooldown, exists := r.cooldowns[mis.m.Author.ID]; exists {
				if cooldown.After(time.Now()) {
					if err := mis.s.ChannelMessageDelete(mis.m.ChannelID, mis.m.ID); err != nil {
						r.l.Errorf("error deleting message: %v", err)
					}
					newCooldown := cooldown.Add(1 * time.Second)
					cooldownLeft := time.Until(newCooldown)
					if cooldownLeft > time.Duration(r.settings[mis.m.GuildID].SpamCooldownMax) {
						newCooldown = time.Now().Add(time.Duration(r.settings[mis.m.GuildID].SpamCooldownMax))
						cooldownLeft = time.Duration(r.settings[mis.m.GuildID].SpamCooldownMax)
					}
					r.cooldowns[mis.m.Author.ID] = newCooldown
					r.l.Infof("Cooldown not expired for %v; new cooldown %v; deleting message %v: %v", mis.m.Author.String(), cooldownLeft, mis.m.Author.String(), mis.m.Content)
					continue
				} else {
					delete(r.cooldowns, mis.m.Author.ID)
				}
			}
			kept, err := r.censor(mis.s, mis.m)
			if err != nil {
				r.l.Errorf("error censoring message: %v", err)
				r.cancel()
			}
			if !kept {
				continue
			}
			r.redis.Set(fmt.Sprintf("%s:%s", mis.m.Author.ID, mis.m.ID), "", time.Duration(r.settings[mis.m.GuildID].SpamCooldownMax))
			keys, err := r.redis.Keys(fmt.Sprintf("%s:*", mis.m.Author.ID)).Result()
			if err != nil {
				return fmt.Errorf("listing redis keys")
			}
			switch {
			case len(keys) > 3:
				r.cooldowns[mis.m.Author.ID] = time.Now().Add(time.Duration(r.settings[mis.m.GuildID].SpamCooldownInitial))
				maxDur := time.Duration(0)
				for _, key := range keys {
					dur, err := r.redis.TTL(key).Result()
					if err != nil {
						return fmt.Errorf("error getting TTL for key %v: %v", key, err)
					}
					if dur > maxDur {
						maxDur = dur
					}
				}
				r.l.Infof("Spammer detected; disabling quick chat for %v", r.settings[mis.m.GuildID].SpamCooldownInitial)
				if err := mis.s.ChannelMessageDelete(mis.m.ChannelID, mis.m.ID); err != nil {
					r.l.Errorf("error deleting message: %v", err)
					r.cancel()
				}
				r.l.Infof("Deleted message %v: %q", mis.m.Author.String(), mis.m.Content)
				if _, err := mis.s.ChannelMessageSend(mis.m.ChannelID, fmt.Sprintf("Quick chat disabled for %v (%v messages last %v)", (time.Duration)(r.settings[mis.m.GuildID].SpamCooldownInitial).String(), len(keys), time.Duration(r.settings[mis.m.GuildID].SpamCooldownMax).String())); err != nil {
					r.l.Errorf("error sending message: %v", err)
				}
			default:
			}
		case <-ctx.Done():
			return nil
		}
	}
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func (r rlCensor) messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	// Only handle messages in specific channel
	_, exists := r.settings[m.GuildID]
	if !exists || m.ChannelID != r.settings[m.GuildID].ChannelID {
		return
	}

	r.l.Infof("handling message %v: %#v", m.ID, *m.Message)
	r.handlerC <- messageInSession{s, m.Message}
}

func (r rlCensor) censor(s *discordgo.Session, m *discordgo.Message) (bool, error) {
	if _, exists := r.settings[m.GuildID]; !exists {
		return false, nil
	}
	r.l.Infof("Censoring message %v: %q", m.Author.String(), m.Content)
	if _, ok := allowedMessageMap[m.Content]; !ok {
		if err := s.MessageReactionAdd(m.ChannelID, m.ID, r.settings[m.GuildID].VotenoEmojiID); err != nil {
			if restErr, ok := err.(*discordgo.RESTError); !ok || restErr.Message.Code != discordgo.ErrCodeUnknownEmoji {
				return false, fmt.Errorf("error reacting to message: %v", err)
			}
		}
		go func() {
			time.Sleep(time.Duration(r.settings[m.GuildID].DeleteDelay))
			if err := s.ChannelMessageDelete(m.ChannelID, m.ID); err != nil {
				r.l.Errorf("error deleting message: %v", err)
				r.cancel()
			}
			r.l.Infof("Deleted message %v: %q", m.Author.String(), m.Content)
		}()
		return false, nil
	}
	return true, nil
}
