#!/bin/bash
set -euo pipefail
export h="$(sha512sum --text < build/index.html | cut -c -12)"
docker stack deploy --prune --compose-file docker-compose.yml discord
